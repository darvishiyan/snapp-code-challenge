package com.darvishiyan.snappcodechallenge.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.darvishiyan.snappcodechallenge.model.LocationPermissionStatus

@Suppress("UNCHECKED_CAST")
class EventHandler {

    /**
     * observe in main
     */
    //<editor-fold desc="Request Location Permission">
    private val _requestLocationPermission = SingleLiveEvent<Any>()

    val requestLocationPermissionObserver: LiveData<Any>
        get() = _requestLocationPermission as LiveData<Any>

    fun requestLocationPermission() {
        _requestLocationPermission.call()
    }
    //</editor-fold>

    /**
     * send from main
     */
    //<editor-fold desc="Location Permission Status">
    private val _locationPermission = MutableLiveData<Event<LocationPermissionStatus>>()

    val locationPermissionStatusObserver: LiveData<Event<LocationPermissionStatus>>
        get() = _locationPermission

    fun locationPermissionStatusEvent(status: LocationPermissionStatus) {
        _locationPermission.value = Event(status)
    }
    //</editor-fold>
}