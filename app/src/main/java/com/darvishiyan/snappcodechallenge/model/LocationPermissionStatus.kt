package com.darvishiyan.snappcodechallenge.model

enum class LocationPermissionStatus {
    Allow,
    Deny
}