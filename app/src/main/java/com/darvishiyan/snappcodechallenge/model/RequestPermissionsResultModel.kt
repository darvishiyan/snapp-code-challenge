package com.darvishiyan.snappcodechallenge.model

data class RequestPermissionsResultModel(
    val requestCode: Int,
    val permissions: Array<out String>,
    val grantResults: IntArray
)