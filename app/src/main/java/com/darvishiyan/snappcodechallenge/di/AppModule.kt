package com.darvishiyan.snappcodechallenge.di

import android.content.Context
import android.content.SharedPreferences
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import com.darvishiyan.snappcodechallenge.util.EventHandler
import io.reactivex.disposables.CompositeDisposable
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val appModule = module {

    single { EventHandler() }

    factory {
        CompositeDisposable()
    }

    single<SharedPreferences> {
        androidApplication().getSharedPreferences("App", Context.MODE_PRIVATE)
    }

    factory<Animation>(Qualifiers.fadeInAnimation) {
        AlphaAnimation(0f, 1f).apply {
            startOffset = 500
            duration = 700
        }
    }

}