package com.darvishiyan.snappcodechallenge.di

import androidx.room.Room
import com.darvishiyan.snappcodechallenge.BuildConfig
import com.darvishiyan.snappcodechallenge.dataaccess.local.VehicleLocalDataSource
import com.darvishiyan.snappcodechallenge.dataaccess.model.database.AppDatabase
import com.darvishiyan.snappcodechallenge.dataaccess.remote.ApiService
import com.darvishiyan.snappcodechallenge.dataaccess.remote.VehicleRemoteDataSource
import com.darvishiyan.snappcodechallenge.dataaccess.repository.VehicleRepository
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


val dataAccessModule = module {

    single<Interceptor>(Qualifiers.loggerInterceptor) {
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    single {
        OkHttpClient().newBuilder()
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .also {
                if (BuildConfig.DEBUG) {
                    it.addInterceptor(get<Interceptor>(Qualifiers.loggerInterceptor))
                }
            }
            .build()
    }

    single<Converter.Factory> {
        GsonConverterFactory.create(
            GsonBuilder()
                .setPrettyPrinting()
                .create()
        )
    }

    single<CallAdapter.Factory> {
        RxJava2CallAdapterFactory.create()
    }

    single<Retrofit> {
        Retrofit.Builder()
            .baseUrl("https://snapp.ir/")
            .client(get())
            .addConverterFactory(get())
            .addCallAdapterFactory(get())
            .build()
    }

    factory<ApiService> {
        get<Retrofit>().create(ApiService::class.java)
    }

    factory { VehicleLocalDataSource(get()) }
    factory { VehicleRemoteDataSource(get()) }
    factory { VehicleRepository(androidContext(), get(), get()) }

    single { Room.databaseBuilder(androidContext(), AppDatabase::class.java, "appDB").build() }

    factory { get<AppDatabase>().vehicleDao() }

}