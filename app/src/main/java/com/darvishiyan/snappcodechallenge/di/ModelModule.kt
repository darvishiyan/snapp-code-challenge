package com.darvishiyan.snappcodechallenge.di

import androidx.appcompat.app.AppCompatActivity
import com.darvishiyan.snappcodechallenge.ui.home.model.HomeModel
import com.darvishiyan.snappcodechallenge.ui.main.model.MainModel
import com.darvishiyan.snappcodechallenge.ui.permission.model.PermissionModel
import org.koin.dsl.module

val modelModule = module {

    factory { (activity: AppCompatActivity) -> MainModel(activity, get()) }
    factory { PermissionModel(get()) }
    factory { HomeModel(get(), get()) }

}