package com.darvishiyan.snappcodechallenge.di

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.darvishiyan.snappcodechallenge.ui.home.viewmodel.HomeViewModel
import com.darvishiyan.snappcodechallenge.ui.main.viewmodel.MainViewModel
import com.darvishiyan.snappcodechallenge.ui.permission.viewmodel.PermissionViewModel
import com.darvishiyan.snappcodechallenge.ui.splash.viewmodel.SplashViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { (activity: AppCompatActivity) -> MainViewModel(get { parametersOf(activity) }) }
    viewModel { SplashViewModel(androidContext()) }
    viewModel { (fragment: Fragment) -> HomeViewModel(fragment, get()) }
    viewModel { PermissionViewModel(get()) }

}