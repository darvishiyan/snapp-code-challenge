package com.darvishiyan.snappcodechallenge.di

import org.koin.core.qualifier.named

object Qualifiers {

    val loggerInterceptor = named("LoggerInterceptor")

    val fadeInAnimation = named("FadeInAnimation")

}