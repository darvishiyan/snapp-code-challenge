package com.darvishiyan.snappcodechallenge.core

import androidx.lifecycle.LifecycleOwner
import org.koin.core.KoinComponent

open class BaseModel : KoinComponent {

    open fun onLifecycleOwnerBounded(viewLifecycleOwner: LifecycleOwner) {}

}