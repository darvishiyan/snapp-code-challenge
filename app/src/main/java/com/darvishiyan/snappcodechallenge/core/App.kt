package com.darvishiyan.snappcodechallenge.core

import android.app.Application
import com.darvishiyan.snappcodechallenge.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.logger.AndroidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin()
    }

    private fun startKoin() {
        startKoin {
            androidContext(this@App)
            logger(AndroidLogger(Level.ERROR))
            modules(
                listOf(
                    appModule,
                    viewModelModule,
                    modelModule,
                    dataAccessModule,
                    adapterModule
                )
            )
        }
    }
}