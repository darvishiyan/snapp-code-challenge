package com.darvishiyan.snappcodechallenge.core

import androidx.databinding.ViewDataBinding
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.FragmentNavigator

open class NavigationBaseViewModel<DB : ViewDataBinding> : BaseViewModel<DB>() {

    internal lateinit var navigator: NavController

    open fun onBindNavigator() {}

    fun goto(directions: NavDirections) {
        navigator.navigate(directions)
    }

    fun goto(directions: NavDirections, extras: FragmentNavigator.Extras) {
        navigator.navigate(directions, extras)
    }

    open fun goBack() {
        navigator.navigateUp()
    }
}