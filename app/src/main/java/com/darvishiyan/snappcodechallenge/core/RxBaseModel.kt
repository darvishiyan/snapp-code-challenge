package com.darvishiyan.snappcodechallenge.core

import io.reactivex.disposables.CompositeDisposable

open class RxBaseModel(
    private val compositeDisposable: CompositeDisposable
) : BaseModel() {

    fun disposeComposite() {
        compositeDisposable.dispose()
    }
}