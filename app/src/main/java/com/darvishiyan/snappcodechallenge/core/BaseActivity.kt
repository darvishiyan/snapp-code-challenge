package com.darvishiyan.snappcodechallenge.core

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import com.darvishiyan.snappcodechallenge.BR

abstract class BaseActivity<VM : BaseViewModel<DB>, DB : ViewDataBinding> : AppCompatActivity() {

    open val viewModelId: Int = BR.vm
    abstract val viewModel: VM
    abstract val layoutResId: Int
    lateinit var binding: DB
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = bindView(viewModel)
        onViewBounded(binding.root, binding, savedInstanceState)
    }

    private fun bindView(viewModel: VM): DB {
        return DataBindingUtil.setContentView<DB>(this, layoutResId).apply {
            setVariable(viewModelId, viewModel)
            lifecycleOwner = this@BaseActivity
            onLifecycleOwnerBounded(this@BaseActivity)
            executePendingBindings()
            onViewBounded()
        }
    }

    abstract fun onViewBounded()

    open fun onLifecycleOwnerBounded(viewLifecycleOwner: LifecycleOwner) {
        viewModel.onLifecycleOwnerBounded(viewLifecycleOwner)
    }

    private fun onViewBounded(view: View, binding: DB, savedInstanceState: Bundle?) {
        lifecycle.addObserver(viewModel)
        viewModel.onBindView(binding, savedInstanceState)
        viewModel.onViewBounded(view, savedInstanceState)
    }
}