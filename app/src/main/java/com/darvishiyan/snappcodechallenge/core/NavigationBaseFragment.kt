package com.darvishiyan.snappcodechallenge.core

import android.os.Bundle
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.navigation.Navigation

abstract class NavigationBaseFragment<VM : BaseViewModel<DB>, DB : ViewDataBinding> :
    BaseFragment<VM, DB>() {

    override fun onViewBounded(view: View, savedInstanceState: Bundle?) {
        super.onViewBounded(view, savedInstanceState)
        val navController = Navigation.findNavController(view)
        (viewModel as? NavigationBaseViewModel<*>)?.apply {
            navigator = navController
            onBindNavigator()
        }
    }

}