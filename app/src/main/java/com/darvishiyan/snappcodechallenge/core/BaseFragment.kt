package com.darvishiyan.snappcodechallenge.core

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.transition.TransitionInflater
import com.darvishiyan.snappcodechallenge.BR

abstract class BaseFragment<VM : BaseViewModel<DB>, DB : ViewDataBinding> : Fragment() {

    //<editor-fold desc="Variable">
    open val transitionAnimationResId = android.R.transition.move
    open val viewModelId: Int = BR.vm
    abstract val viewModel: VM
    abstract val layoutResId: Int
    lateinit var binding: DB
        private set
    //</editor-fold>

    //<editor-fold desc="View Handler">
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(transitionAnimationResId)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return onBindView(inflater, container, savedInstanceState)
    }

    private fun onBindView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = bindView(inflater, layoutResId, container, viewModel)
        viewModel.onBindView(binding, savedInstanceState)
        return binding.root
    }

    private fun bindView(
        inflater: LayoutInflater,
        layoutResId: Int,
        container: ViewGroup?,
        viewModel: VM
    ): DB {
        return DataBindingUtil.inflate<DB>(
            inflater,
            layoutResId,
            container,
            false
        ).apply {
            setVariable(viewModelId, viewModel)
            lifecycleOwner = viewLifecycleOwner
            onLifecycleOwnerBounded(viewLifecycleOwner)
            executePendingBindings()
        }
    }

    private fun onLifecycleOwnerBounded(viewLifecycleOwner: LifecycleOwner) {
        viewModel.onLifecycleOwnerBounded(viewLifecycleOwner)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onViewBounded(view, savedInstanceState)
    }

    open fun onViewBounded(view: View, savedInstanceState: Bundle?) {
        lifecycle.addObserver(viewModel)
        viewModel.onViewBounded(view, savedInstanceState)
    }
    //</editor-fold>
}