package com.darvishiyan.snappcodechallenge.ui.home.model

import com.darvishiyan.snappcodechallenge.core.RxBaseModel
import com.darvishiyan.snappcodechallenge.dataaccess.model.server.VehicleItem
import com.darvishiyan.snappcodechallenge.dataaccess.repository.VehicleRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

class HomeModel(
    private val repository: VehicleRepository,
    private val compositeDisposable: CompositeDisposable
) : RxBaseModel(compositeDisposable) {

    fun loadVehicle(result: (List<VehicleItem>) -> Unit) = repository.getVehicle()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(result, Throwable::printStackTrace)
        .addTo(compositeDisposable)

}