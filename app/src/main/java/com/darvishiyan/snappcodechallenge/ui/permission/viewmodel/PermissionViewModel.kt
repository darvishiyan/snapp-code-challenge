package com.darvishiyan.snappcodechallenge.ui.permission.viewmodel

import android.view.animation.Animation
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.observe
import androidx.navigation.fragment.FragmentNavigatorExtras
import com.darvishiyan.snappcodechallenge.core.NavigationBaseViewModel
import com.darvishiyan.snappcodechallenge.databinding.FragmentPermissionBinding
import com.darvishiyan.snappcodechallenge.di.Qualifiers
import com.darvishiyan.snappcodechallenge.model.LocationPermissionStatus
import com.darvishiyan.snappcodechallenge.ui.permission.model.PermissionModel
import com.darvishiyan.snappcodechallenge.ui.permission.view.PermissionFragmentDirections
import org.koin.core.inject

class PermissionViewModel(
    private val model: PermissionModel
) : NavigationBaseViewModel<FragmentPermissionBinding>() {

    val fadeInAnimation: Animation by inject(Qualifiers.fadeInAnimation)

    override fun onLifecycleOwnerBounded(viewLifecycleOwner: LifecycleOwner) {
        super.onLifecycleOwnerBounded(viewLifecycleOwner)
        model.onLifecycleOwnerBounded(viewLifecycleOwner)
        model.permissionStatus.observe(viewLifecycleOwner) {
            if (it == LocationPermissionStatus.Allow) when {
                else -> goto(
                    PermissionFragmentDirections.actionPermissionFragmentToHomeFragment(),
                    FragmentNavigatorExtras(
                        binding.message to "message"
                    )
                )
            }
        }
    }

    fun getPermission() = model.getPermission()

}