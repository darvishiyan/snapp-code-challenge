package com.darvishiyan.snappcodechallenge.ui.permission.model

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.observe
import com.darvishiyan.snappcodechallenge.core.BaseModel
import com.darvishiyan.snappcodechallenge.model.LocationPermissionStatus
import com.darvishiyan.snappcodechallenge.util.EventHandler


class PermissionModel(
    private val eventHandler: EventHandler
) : BaseModel() {

    var permissionStatus = MutableLiveData<LocationPermissionStatus>()

    override fun onLifecycleOwnerBounded(viewLifecycleOwner: LifecycleOwner) {
        super.onLifecycleOwnerBounded(viewLifecycleOwner)
        eventHandler.locationPermissionStatusObserver.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let {
                permissionStatus.value = it
            }
        }
    }

    fun getPermission() = eventHandler.requestLocationPermission()
}