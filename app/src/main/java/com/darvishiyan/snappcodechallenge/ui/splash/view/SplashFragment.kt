package com.darvishiyan.snappcodechallenge.ui.splash.view

import com.darvishiyan.snappcodechallenge.R
import com.darvishiyan.snappcodechallenge.core.NavigationBaseFragment
import com.darvishiyan.snappcodechallenge.databinding.FragmentSplashBinding
import com.darvishiyan.snappcodechallenge.ui.splash.viewmodel.SplashViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class SplashFragment : NavigationBaseFragment<SplashViewModel, FragmentSplashBinding>() {
    override val viewModel: SplashViewModel by viewModel()
    override val layoutResId: Int = R.layout.fragment_splash
}