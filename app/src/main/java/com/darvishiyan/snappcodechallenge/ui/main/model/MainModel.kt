package com.darvishiyan.snappcodechallenge.ui.main.model

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.observe
import com.darvishiyan.snappcodechallenge.core.BaseModel
import com.darvishiyan.snappcodechallenge.model.LocationPermissionStatus
import com.darvishiyan.snappcodechallenge.model.RequestPermissionsResultModel
import com.darvishiyan.snappcodechallenge.util.EventHandler

class MainModel(
    private val activity: AppCompatActivity,
    private val eventHandler: EventHandler
) : BaseModel() {

    private val accessFineLocationRequestCode = 2000

    fun initObserver(lifecycleOwner: LifecycleOwner) {
        eventHandler.requestLocationPermissionObserver.observe(lifecycleOwner) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    accessFineLocationRequestCode
                )
            }
        }
    }

    fun onRequestPermissionsResultEvent(result: RequestPermissionsResultModel) {
        if (result.requestCode == accessFineLocationRequestCode) {
            if (result.grantResults.contains(PackageManager.PERMISSION_GRANTED))
                eventHandler.locationPermissionStatusEvent(LocationPermissionStatus.Allow)
            else
                eventHandler.locationPermissionStatusEvent(LocationPermissionStatus.Deny)
        }
    }
}