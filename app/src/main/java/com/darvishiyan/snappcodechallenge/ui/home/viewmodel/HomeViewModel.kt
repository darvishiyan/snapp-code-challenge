package com.darvishiyan.snappcodechallenge.ui.home.viewmodel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.Fragment
import com.darvishiyan.snappcodechallenge.R
import com.darvishiyan.snappcodechallenge.core.NavigationBaseViewModel
import com.darvishiyan.snappcodechallenge.databinding.FragmentHomeBinding
import com.darvishiyan.snappcodechallenge.ui.home.model.HomeModel
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.markerview.MarkerView
import com.mapbox.mapboxsdk.plugins.markerview.MarkerViewManager
import com.squareup.picasso.Picasso

class HomeViewModel(
    private val fragment: Fragment,
    private val model: HomeModel
) : NavigationBaseViewModel<FragmentHomeBinding>() {

    override fun onBindView(binding: FragmentHomeBinding, savedInstanceState: Bundle?) {
        super.onBindView(binding, savedInstanceState)
        with(binding.map) {
            onCreate(savedInstanceState)
            getMapAsync { map ->
                map.setStyle(Style.MAPBOX_STREETS)
                map.cameraPosition =
                    CameraPosition.Builder()
                        .target(
                            LatLng(35.6983714, 51.4465242)
                        ).build()
            }
        }
    }

    override fun onViewBounded(view: View, savedInstanceState: Bundle?) {
        super.onViewBounded(view, savedInstanceState)
        model.loadVehicle { data ->
            binding.map.getMapAsync {
                val markerViewManager = MarkerViewManager(binding.map, it)
                data.forEach { item ->
                    val marker = MarkerView(
                        LatLng(item.lat, item.lng),
                        LayoutInflater.from(fragment.context).inflate(
                            R.layout.marker, null, false
                        ).apply {
                            Picasso.get()
                                .load(item.imageUrl)
                                .rotate(item.bearing.toFloat(), .5f, .5f)
                                .into(findViewById<AppCompatImageView>(R.id.iv))
                        }
                    )
                    markerViewManager.addMarker(marker)
                }
            }
        }
    }

}