package com.darvishiyan.snappcodechallenge.ui.permission.view

import com.darvishiyan.snappcodechallenge.R
import com.darvishiyan.snappcodechallenge.core.NavigationBaseFragment
import com.darvishiyan.snappcodechallenge.databinding.FragmentPermissionBinding
import com.darvishiyan.snappcodechallenge.ui.permission.viewmodel.PermissionViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class PermissionFragment :
    NavigationBaseFragment<PermissionViewModel, FragmentPermissionBinding>() {
    override val viewModel: PermissionViewModel by viewModel()
    override val layoutResId: Int = R.layout.fragment_permission
}