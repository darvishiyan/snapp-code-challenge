package com.darvishiyan.snappcodechallenge.ui.home.view

import android.os.Bundle
import com.darvishiyan.snappcodechallenge.R
import com.darvishiyan.snappcodechallenge.core.NavigationBaseFragment
import com.darvishiyan.snappcodechallenge.databinding.FragmentHomeBinding
import com.darvishiyan.snappcodechallenge.ui.home.viewmodel.HomeViewModel
import com.mapbox.mapboxsdk.Mapbox
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class HomeFragment : NavigationBaseFragment<HomeViewModel, FragmentHomeBinding>() {
    override val viewModel: HomeViewModel by viewModel { parametersOf(this) }
    override val layoutResId: Int = R.layout.fragment_home

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initMap()
    }

    private fun initMap() {
        context?.let {
            Mapbox.getInstance(
                it,
                "pk.eyJ1IjoiZGFydmlzaGl5YW4iLCJhIjoiY2thc2M2eXR0MHI2MDJzcHJydTRqMThrcSJ9.tYFgq94bcAanLQB27DNzCA"
            )
        }
    }

    override fun onStart() {
        super.onStart()
        binding.map.onStart()
    }

    override fun onResume() {
        super.onResume()
        binding.map.onResume()
    }

    override fun onPause() {
        super.onPause()
        binding.map.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        binding.map.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.map.onDestroy()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.map.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        binding.map.onSaveInstanceState(outState)
    }
}