package com.darvishiyan.snappcodechallenge.ui.splash.viewmodel

import android.content.Context
import android.os.Bundle
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.FragmentNavigatorExtras
import com.darvishiyan.snappcodechallenge.core.NavigationBaseViewModel
import com.darvishiyan.snappcodechallenge.databinding.FragmentSplashBinding
import com.darvishiyan.snappcodechallenge.ui.splash.view.SplashFragmentDirections
import com.darvishiyan.snappcodechallenge.util.hasLocationPermission
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashViewModel(
    private val context: Context
) : NavigationBaseViewModel<FragmentSplashBinding>() {
    override fun onBindView(binding: FragmentSplashBinding, savedInstanceState: Bundle?) {
        super.onBindView(binding, savedInstanceState)
        viewModelScope.launch {
            delay(1500)
            val extras = FragmentNavigatorExtras(
                binding.message to "message"
            )
            when {
                !hasLocationPermission(context) -> gotoPermissionFragment(extras)
                else -> gotoHomeFragment(extras)
            }
        }
    }

    private fun gotoHomeFragment(extras: FragmentNavigator.Extras) {
        goto(
            SplashFragmentDirections.actionSplashFragmentToHomeFragment(),
            extras
        )
    }

    private fun gotoPermissionFragment(extras: FragmentNavigator.Extras) {
        goto(
            SplashFragmentDirections.actionSplashFragmentToPermissionFragment(),
            extras
        )
    }
}