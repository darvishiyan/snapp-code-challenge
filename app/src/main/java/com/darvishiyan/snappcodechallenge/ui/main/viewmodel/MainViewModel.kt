package com.darvishiyan.snappcodechallenge.ui.main.viewmodel

import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import com.darvishiyan.snappcodechallenge.core.BaseViewModel
import com.darvishiyan.snappcodechallenge.model.RequestPermissionsResultModel
import com.darvishiyan.snappcodechallenge.ui.main.model.MainModel

class MainViewModel(
    private val model: MainModel
) : BaseViewModel<ViewDataBinding>() {

    fun onRequestPermissionsResultEvent(result: RequestPermissionsResultModel) =
        model.onRequestPermissionsResultEvent(result)

    fun initObserver(lifecycleOwner: LifecycleOwner) {
        model.initObserver(lifecycleOwner)
    }
}