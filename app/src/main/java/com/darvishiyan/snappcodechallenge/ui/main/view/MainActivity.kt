package com.darvishiyan.snappcodechallenge.ui.main.view

import androidx.databinding.ViewDataBinding
import com.darvishiyan.snappcodechallenge.R
import com.darvishiyan.snappcodechallenge.core.BaseActivity
import com.darvishiyan.snappcodechallenge.model.RequestPermissionsResultModel
import com.darvishiyan.snappcodechallenge.ui.main.viewmodel.MainViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class MainActivity : BaseActivity<MainViewModel, ViewDataBinding>() {
    override val viewModel: MainViewModel by viewModel { parametersOf(this) }
    override val layoutResId: Int = R.layout.activity_main

    override fun onViewBounded() {
        viewModel.initObserver(this)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        viewModel.onRequestPermissionsResultEvent(
            RequestPermissionsResultModel(
                requestCode,
                permissions,
                grantResults
            )
        )
    }

    override fun onRestart() {
        super.onRestart()
        viewModel.initObserver(this)
    }
}