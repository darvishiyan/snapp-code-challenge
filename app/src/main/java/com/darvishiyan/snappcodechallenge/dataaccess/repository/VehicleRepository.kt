package com.darvishiyan.snappcodechallenge.dataaccess.repository

import android.content.Context
import com.darvishiyan.snappcodechallenge.dataaccess.local.VehicleLocalDataSource
import com.darvishiyan.snappcodechallenge.dataaccess.model.server.VehicleItem
import com.darvishiyan.snappcodechallenge.dataaccess.remote.VehicleRemoteDataSource
import com.darvishiyan.snappcodechallenge.util.internetIsConnected
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class VehicleRepository(
    private val context: Context,
    private val remote: VehicleRemoteDataSource,
    private val local: VehicleLocalDataSource
) {
    fun getVehicle(): Observable<List<VehicleItem>> {
        return if (internetIsConnected(context)) {
            remote.getVehicle()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap { data ->
                    local.storeVehicle(data.items)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .andThen(Observable.just(data.items))
                }
        } else
            local.getVehicle()
    }
}