package com.darvishiyan.snappcodechallenge.dataaccess.remote

import com.darvishiyan.snappcodechallenge.dataaccess.model.server.Vehicle
import io.reactivex.Observable

class VehicleRemoteDataSource(
    private val api: ApiService
) {
    fun getVehicle(): Observable<Vehicle> = api.getVehicles().toObservable()
}