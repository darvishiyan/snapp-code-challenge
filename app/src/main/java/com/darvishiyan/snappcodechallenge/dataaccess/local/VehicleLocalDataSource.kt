package com.darvishiyan.snappcodechallenge.dataaccess.local

import com.darvishiyan.snappcodechallenge.dataaccess.model.database.dao.VehicleDao
import com.darvishiyan.snappcodechallenge.dataaccess.model.server.VehicleItem
import com.darvishiyan.snappcodechallenge.dataaccess.model.server.toVehicleEntity
import com.darvishiyan.snappcodechallenge.dataaccess.model.server.toVehicleItem
import io.reactivex.Completable
import io.reactivex.Observable

class VehicleLocalDataSource(
    private val vehicleDao: VehicleDao
) {

    fun getVehicle(): Observable<List<VehicleItem>> {
        return vehicleDao.getAllVehicle().map { data ->
            data.map { it.toVehicleItem() }
        }.toObservable()
    }

    fun storeVehicle(vehicle: List<VehicleItem>): Completable {
        val entity = vehicle.map { it.toVehicleEntity() }.toTypedArray()
        return vehicleDao.insertAllVehicle(*entity)
    }

}