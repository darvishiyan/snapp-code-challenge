package com.darvishiyan.snappcodechallenge.dataaccess.remote

import com.darvishiyan.snappcodechallenge.dataaccess.model.server.Vehicle
import io.reactivex.Single
import retrofit2.http.GET

interface ApiService {

    @GET("assets/test/document.json")
    fun getVehicles(): Single<Vehicle>

}