package com.darvishiyan.snappcodechallenge.dataaccess.model.database.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "Vehicle",
    indices = [Index(value = ["lat", "lng"], unique = true)]
)
data class VehicleEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,
    val type: String,
    val lat: Double,
    val lng: Double,
    val bearing: Int,
    val imageUrl: String
)