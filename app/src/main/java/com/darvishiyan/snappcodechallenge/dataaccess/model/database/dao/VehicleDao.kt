package com.darvishiyan.snappcodechallenge.dataaccess.model.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.darvishiyan.snappcodechallenge.dataaccess.model.database.model.VehicleEntity
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface VehicleDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAllVehicle(vararg entity: VehicleEntity): Completable

    @Query("SELECT * FROM Vehicle")
    fun getAllVehicle(): Single<List<VehicleEntity>>

}