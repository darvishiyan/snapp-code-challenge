package com.darvishiyan.snappcodechallenge.dataaccess.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.darvishiyan.snappcodechallenge.dataaccess.model.database.dao.VehicleDao
import com.darvishiyan.snappcodechallenge.dataaccess.model.database.model.VehicleEntity

@Database(
    entities = [
        VehicleEntity::class
    ], version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun vehicleDao(): VehicleDao

}