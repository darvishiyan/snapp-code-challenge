package com.darvishiyan.snappcodechallenge.dataaccess.model.server

import android.os.Parcelable
import com.darvishiyan.snappcodechallenge.dataaccess.model.database.model.VehicleEntity
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Vehicle(
    @SerializedName("vehicles")
    val items: List<VehicleItem>
) : Parcelable

@Parcelize
data class VehicleItem(
    val type: String,
    val lat: Double,
    val lng: Double,
    val bearing: Int,
    @SerializedName("image_url")
    val imageUrl: String
) : Parcelable

fun VehicleEntity.toVehicleItem() = VehicleItem(type, lat, lng, bearing, imageUrl)

fun VehicleItem.toVehicleEntity() =
    VehicleEntity(type = type, lat = lat, lng = lng, bearing = bearing, imageUrl = imageUrl)